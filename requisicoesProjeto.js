
const express = require("express")  //express retorna uma função que enstancia o express
const bodyParser = require("body-parser")

const app = express()

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

// app.use(express.urlencoded({extended: false}))
// app.use(express.json())

//criando a porta 
const porta = 3001

//mostrando a porta aberta 
app.listen(porta, function(){
    console.log(`servidor rodando na porta ${porta}`)
})


//criando a rota do home
app.get("/home", function(req, resp){
    resp.sendFile(__dirname + "/html/home.html")
})

//criando a rota do sobre
app.get("/sobre", function(req, resp){
    resp.sendFile(__dirname + "/html/sobre.html")
})

//criando a rota do conatato
app.get("/contato", function(req, resp){
    resp.sendFile(__dirname + "/html/contato.html")
})

//envia com metodo post
app.post("/mensagem", function(req,resp){
   console.log(req.body.nmNome) 
    resp.send("formulario recebido metodo post ..." + req.body.nmNome)
})

//envia com o metodo get
// app.get("/mensagem", function(req,resp){
//     console.log(req.query)   //query para o metodo get
//      resp.send("formulario recebido...")
//  })
 
